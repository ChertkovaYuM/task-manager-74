package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDTO, String> {

    @NotNull
    List<ProjectDTO> findAll(@NotNull Sort sort);

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

    @Modifying
    @Transactional
    void deleteByUserId(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}
