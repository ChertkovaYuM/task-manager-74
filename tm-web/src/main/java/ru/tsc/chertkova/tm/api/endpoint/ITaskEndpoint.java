package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    TaskDTO save(
            @NotNull
            @WebParam(name = "TaskDTO")
            @RequestBody TaskDTO TaskDTO
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "TaskDTO")
            @RequestBody TaskDTO TaskDTO
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "TasksDTO")
            @RequestBody List<TaskDTO> TasksDTO
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

}
