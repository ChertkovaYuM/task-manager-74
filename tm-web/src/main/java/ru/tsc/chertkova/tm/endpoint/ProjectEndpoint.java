package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/create")
    public void create() {
        projectService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return projectService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDTO save(
            @NotNull
            @WebParam(name = "ProjectDTO")
            @RequestBody final ProjectDTO ProjectDTO
    ) {
        return projectService.save(UserUtil.getUserId(), ProjectDTO);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "ProjectDTO")
            @RequestBody final ProjectDTO ProjectDTO
    ) {
        projectService.delete(UserUtil.getUserId(), ProjectDTO);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "ProjectDTOs")
            @RequestBody final List<ProjectDTO> ProjectDTOs) {
        projectService.deleteAll(UserUtil.getUserId(), ProjectDTOs);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id) {
        projectService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count(UserUtil.getUserId());
    }

}
