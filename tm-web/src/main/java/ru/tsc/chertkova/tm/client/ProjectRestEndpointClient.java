package ru.tsc.chertkova.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;

import java.util.Arrays;
import java.util.List;

public class ProjectRestEndpointClient implements IProjectEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/project/";

    @Override
    public void create() {
        @NotNull final String localUrl = "create";
    }

    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, ProjectDTO[].class));
    }

    @Override
    public ProjectDTO findById(@NotNull final String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, ProjectDTO.class, id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public ProjectDTO save(@NotNull final ProjectDTO project) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        return template.postForObject(ROOT_URL + localUrl, entity, ProjectDTO.class);

    }

    @Override
    public void delete(@NotNull final ProjectDTO project) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(ROOT_URL + localUrl, entity, ProjectDTO.class);
    }

    @Override
    public void deleteAll(@NotNull final List<ProjectDTO> projects) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<ProjectDTO>> entity = new HttpEntity<>(projects, headers);
        template.postForObject(ROOT_URL + localUrl, entity, ProjectDTO[].class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    public static void main(String[] args) {
        ProjectRestEndpointClient client = new ProjectRestEndpointClient();

        @NotNull final List<ProjectDTO> projects = client.findAll();
        System.out.println("Список проектов:");
        for (@NotNull final ProjectDTO project : projects) {
            System.out.printf("Проект: %s; ID: %s%n", project.getName(), project.getId());
        }
        System.out.println();

        @NotNull final String id = projects.get(0).getId();

        System.out.printf("Количество проектов: %d%n", client.count());
        System.out.printf("Существование проекта: %b%n", client.existsById(id));

        @Nullable final ProjectDTO project = client.findById(id);
        System.out.printf("Поиск проекта: %s%n", project.getName());

        client.deleteById(id);
        System.out.printf("Количество проектов: %d%n", client.count());
        System.out.printf("Существование проекта: %b%n", client.existsById(id));

        client.save(project);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.delete(project);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.save(project);
        client.deleteAll(projects);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.save(project);
        System.out.printf("Количество проектов: %d%n", client.count());
        client.clear();
        System.out.printf("Количество проектов: %d%n", client.count());
    }

}
