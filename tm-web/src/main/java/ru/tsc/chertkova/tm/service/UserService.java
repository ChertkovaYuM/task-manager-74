package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.enumerated.RoleType;
import ru.tsc.chertkova.tm.exception.user.*;
import ru.tsc.chertkova.tm.model.dto.RoleDTO;
import ru.tsc.chertkova.tm.model.dto.UserDTO;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", "admin@domain.com", RoleType.ADMIN);
        initUser("test", "test", "test@domain.com", RoleType.USUAL);
        initUser("user", "user", "user@domain.com", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType role
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (email == null || email.isEmpty()) throw new UserEmailEmptyException();
        if (role == null) throw new UserRoleEmptyException();
        final UserDTO user = userRepository.findFirstByLogin(login);
        if (user != null) return;
        createUser(login, password, email, role);
    }

    @Modifying
    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (email == null || email.isEmpty()) throw new UserEmailEmptyException();
        if (roleType == null) throw new UserRoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, email);
        @NotNull final RoleDTO role = new RoleDTO(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @NotNull
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        final UserDTO user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException(login);
        return user;
    }

    @NotNull
    public UserDTO findById(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        final UserDTO user = userRepository.findFirstById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    public UserDTO delete(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        final UserDTO user = userRepository.findFirstById(userId);
        if (user == null) throw new UserNotFoundException();
        userRepository.deleteById(userId);
        return user;
    }

}
