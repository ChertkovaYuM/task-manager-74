<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp">
 <jsp:param name="title" value="Task - ${task.id}"/>
</jsp:include>
<h1>Edit Task</h1>
<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <table>
        <tr>
            <th>Attribute</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Name:</td>
            <td>
                <form:input type="text" path="name"/>
            </td>
        </tr>
        <tr>
            <td>Status:</td>
            <td>
                <form:select path="status">
                    <form:options items="${enumStatus}" itemLabel="displayName"/>
                </form:select>
            </td>
        </tr>
            <td>Description:</td>
            <td>
                <form:input type="text" path="description"/>
            </td>
        </tr>
        <tr>
            <td>Project:</td>
            <td>
                <form:select path="projectId">
                    <form:option value="${null}" label="--"/>
                    <form:options items="${projects}" itemValue="id" itemLabel="name"/>
                </form:select>
            </td>
        </tr>
        <tr>
            <td>Started:</td>
            <td>
                <form:input type="date" path="dateBegin"/>
            </td>
        </tr>
        <tr>
            <td>Completed:</td>
            <td>
                <form:input type="date" path="dateEnd"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
</form:form>
<jsp:include page="../include/_footer.jsp" />