package ru.tsc.chertkova.tm.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.chertkova.tm.config.ApplicationConfiguration;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/project/";

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private ProjectDTO project1;

    @NotNull
    private String userId;

    @Before
    public void setUp() {
        project = new ProjectDTO();
        project1 = new ProjectDTO();
        project.setName("PROJ");
        project1.setName("PROJ_1");
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        Object principal;
        Object credentials;
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectService.save(userId, project);
        projectService.save(userId, project1);
    }

    @After
    public void tearDown() {
        projectService.clear(userId);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = API_URL + "create";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDTO> projects = projectService.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "http://localhost:8080/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "edit/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void editPost() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "edit/" + projectId;
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = API_URL + "delete/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        Assert.assertNull(projectService.findById(userId, projectId));
    }

}
