package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.chertkova.tm.config.ApplicationConfiguration;
import ru.tsc.chertkova.tm.enumerated.RoleType;
import ru.tsc.chertkova.tm.exception.user.UserLoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.user.UserPasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.UserRoleEmptyException;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.UserDTO;
import ru.tsc.chertkova.tm.service.UserService;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserDtoServiceTest {

    private static final String USER_LOGIN_TEST = "USER_FOR_TEST";

    private static final String USER_PASSWORD_TEST = "USER_FOR_TEST";

    private static final RoleType USER_ROLE_TYPE_TEST = RoleType.USUAL;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void createUser() {
        Assert.assertThrows(UserLoginEmptyException.class,
                () -> userService.createUser("", USER_PASSWORD_TEST, "", USER_ROLE_TYPE_TEST));
        Assert.assertThrows(UserPasswordEmptyException.class,
                () -> userService.createUser(USER_LOGIN_TEST, null, "abc_user@user.ru", USER_ROLE_TYPE_TEST));
        Assert.assertThrows(UserRoleEmptyException.class,
                () -> userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, "", null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, "", USER_ROLE_TYPE_TEST);
        @NotNull final UserDTO userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto.getId());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(UserLoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, "abc_user@user.ru", USER_ROLE_TYPE_TEST);
        @NotNull final UserDTO userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto.getId());
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin("ABC_USER"));
        userService.createUser(USER_LOGIN_TEST, USER_PASSWORD_TEST, "abc_user@user.ru", USER_ROLE_TYPE_TEST);
        @NotNull final UserDTO userDto = userService.findByLogin(USER_LOGIN_TEST);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
        userService.delete(userDto.getId());
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(USER_LOGIN_TEST));
    }

}
