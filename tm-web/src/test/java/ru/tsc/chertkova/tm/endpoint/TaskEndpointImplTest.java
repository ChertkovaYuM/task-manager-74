package ru.tsc.chertkova.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.chertkova.tm.config.ApplicationConfiguration;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskEndpointImplTest {

    @NotNull
    private final static String TASK_API_URL = "http://localhost:8080/api/tasks/";

    @Nullable
    private String userId = null;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext ctx;

    @NotNull
    private TaskDTO task = new TaskDTO();

    private void auth() {
        mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
    }

    @Before
    public void setUp() {
        auth();
        saveTask(task);
    }

    @After
    public void tearDown() {
        cleanUp();
    }

    @SneakyThrows
    private void createTask(@NotNull final TaskDTO taskDto) {
        @NotNull final String url = TASK_API_URL + "create";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void create() throws Exception {
        cleanUp();
        TaskDTO taskDto = new TaskDTO();
        Assert.assertNotNull(userId);
        long count = countTasks();
        Assert.assertEquals(0L, count);
        createTask(taskDto);
        count = countTasks();
        Assert.assertEquals(1L, count);
    }

    @SneakyThrows
    private boolean existsTaskById(@NotNull final String id) {
        @NotNull final String existsByIdUrl = TASK_API_URL + "existsById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        return mapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsById() {
        @NotNull final String taskId = task.getId();
        boolean exists = existsTaskById(taskId);
        Assert.assertTrue(exists);
        Assert.assertFalse(existsTaskById(UUID.randomUUID().toString()));
    }

    @SneakyThrows
    private TaskDTO findTaskById(@NotNull final String id) {
        @NotNull final String findByIdUrl = TASK_API_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(findByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        if (json.isEmpty()) return null;
        return mapper.readValue(json, TaskDTO.class);
    }

    @Test
    public void findById() {
        @NotNull final String taskId = task.getId();
        @Nullable final TaskDTO taskDto = findTaskById(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
    }

    @SneakyThrows
    private List<TaskDTO> findAllTasks() {
        @NotNull final String existsByIdUrl = TASK_API_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        return Arrays.asList(mapper.readValue(json, TaskDTO[].class));
    }

    @Test
    public void findAll() {
        long count = countTasks();
        Assert.assertEquals(1L, count);
        @NotNull final TaskDTO taskDto = findTaskById(task.getId());
        @NotNull final List<TaskDTO> tasks = findAllTasks();
        @NotNull final TaskDTO taskDto1 = tasks.get(0);
        Assert.assertEquals(taskDto.getId(), taskDto1.getId());
    }

    @SneakyThrows
    public long countTasks() {
        @NotNull final String countUrl = TASK_API_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        return mapper.readValue(json, Long.class);
    }

    @Test
    public void count() {
        long count = countTasks();
        Assert.assertEquals(1L, count);
    }

    @SneakyThrows
    private void saveTask(@NotNull final TaskDTO taskDto) {
        @NotNull final String url = TASK_API_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void save() {
        @NotNull final TaskDTO taskDto = new TaskDTO();
        taskDto.setName("TEST");
        Assert.assertNotNull(userId);
        taskDto.setUserId(userId);
        @NotNull final String taskId = taskDto.getId();
        saveTask(taskDto);
        TaskDTO taskDto1 = findTaskById(taskId);
        Assert.assertNotNull(taskDto1);
        Assert.assertEquals(taskId, taskDto1.getId());
        Assert.assertEquals("TEST", taskDto1.getName());
        Assert.assertEquals(userId, taskDto1.getUserId());
    }

    @SneakyThrows
    private void deleteTask(@NotNull final TaskDTO taskDto) {
        @NotNull final String url = TASK_API_URL + "delete";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void delete() {
        long count = countTasks();
        Assert.assertEquals(1L, count);
        deleteTask(task);
        count = countTasks();
        Assert.assertEquals(0L, count);
    }

    @SneakyThrows
    private void deleteTaskById(@NotNull final String id) {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_API_URL + "deleteById/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = task.getId();
        @Nullable TaskDTO taskDto = findTaskById(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
        deleteTaskById(taskId);
        taskDto = findTaskById(taskId);
        Assert.assertNull(taskDto);
    }

    @SneakyThrows
    private void deleteTaskList(@NotNull final List<TaskDTO> tasks) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(
                MockMvcRequestBuilders.post(TASK_API_URL + "deleteAll")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAll() {
        @NotNull final String taskId = task.getId();
        @Nullable TaskDTO taskDto = findTaskById(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
        List<TaskDTO> tasks = new ArrayList<>();
        tasks.add(taskDto);
        deleteTaskList(tasks);
        taskDto = findTaskById(taskId);
        Assert.assertNull(taskDto);
    }

    @SneakyThrows
    private void cleanUp() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_API_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clear() {
        long count = countTasks();
        Assert.assertEquals(1L, count);
        cleanUp();
        count = countTasks();
        Assert.assertEquals(0L, count);
    }

}
