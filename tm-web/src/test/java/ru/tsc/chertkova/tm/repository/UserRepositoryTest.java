package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.config.ApplicationConfiguration;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.UserDTO;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserRepositoryTest {

    @NotNull
    private static final String USER_LOGIN_TEST = "USER_FOR_TEST";

    @NotNull
    private static final String USER_PASSWORD_TEST = "USER_FOR_TEST";


    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    private UserDTO user;

    @Before
    public void setUp() {
        user = new UserDTO();
        user.setLogin(USER_LOGIN_TEST);
        user.setPasswordHash(USER_PASSWORD_TEST);
        repository.save(user);
    }

    @After
    public void tearDown() {
        repository.delete(user);
    }

    @Test
    public void findByLogin() {
        @Nullable final UserDTO userDto = repository.findFirstByLogin(USER_LOGIN_TEST);
        Assert.assertNotNull(userDto);
        Assert.assertEquals(USER_LOGIN_TEST, userDto.getLogin());
    }

}
