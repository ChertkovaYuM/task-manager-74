package ru.tsc.chertkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectShowByIdRequest;
import ru.tsc.chertkova.tm.dto.response.project.ProjectShowByIdResponse;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractProjectListener;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectShowByIdResponse response = projectEndpoint
                .showProjectById(new ProjectShowByIdRequest(getToken(), id));
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

}
