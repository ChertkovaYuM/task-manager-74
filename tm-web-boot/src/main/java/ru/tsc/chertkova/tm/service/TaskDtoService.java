package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.ITaskDtoRepository;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.TaskNotFoundException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.model.dto.TaskDto;

import java.util.Date;
import java.util.List;

@Service
public class TaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Modifying
    @Transactional
    public TaskDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final TaskDto task = new TaskDto(
                "New task: " + System.currentTimeMillis(),
                Status.IN_PROGRESS,
                new Date());
        task.setUserId(userId);
        save(userId, task);
        return task;
    }

    @NotNull
    @Modifying
    @Transactional
    public TaskDto create(@Nullable final String userId,
                          @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final TaskDto task = new TaskDto(
                name + " " + System.currentTimeMillis(),
                Status.NOT_STARTED,
                new Date());
        task.setUserId(userId);
        save(userId, task);
        return task;
    }

    @NotNull
    @Modifying
    @Transactional
    public TaskDto save(@Nullable final String userId,
                        @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        return repository.save(task);
    }

    @Nullable
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    public TaskDto findById(@Nullable final String userId,
                            @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId,
                           @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void delete(@Nullable final String userId,
                       @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        deleteById(userId, task.getId());
    }

    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId,
                          @Nullable final List<TaskDto> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (tasks == null) throw new TaskNotFoundException();
        tasks.stream()
                .forEach(task -> delete(userId, task));
    }

    public boolean existsById(@Nullable final String userId,
                              @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        repository.deleteByUserId(userId);
    }

    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.countByUserId(userId);
    }

}
