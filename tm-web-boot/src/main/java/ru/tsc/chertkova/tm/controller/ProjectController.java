package ru.tsc.chertkova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;
import ru.tsc.chertkova.tm.service.ProjectDtoService;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Nullable
    public List<ProjectDto> getProjects(@NotNull final String userId) {
        return projectService.findAll(UserUtil.getUserId());
    }

    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectService.create(UserUtil.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("projects", "projects", getProjects(UserUtil.getUserId()));
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final ProjectDto project,
            @NotNull final BindingResult result
    ) {
        projectService.save(UserUtil.getUserId(), project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String projectId) {
        @NotNull final ProjectDto project = projectService.findById(UserUtil.getUserId(), projectId);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        projectService.deleteById(UserUtil.getUserId(), id);
        return "redirect:/projects";
    }

}
