package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.IUserDtoRepository;
import ru.tsc.chertkova.tm.exception.user.UserLoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.model.dto.CustomUser;
import ru.tsc.chertkova.tm.model.dto.RoleDto;
import ru.tsc.chertkova.tm.model.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserDtoRepository userRepository;

    @Nullable
    private UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String login) throws UsernameNotFoundException {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException(login);
        @NotNull final User.UserBuilder builder = User.withUsername(login);
        builder.password(user.getPasswordHash());
        @NotNull final List<RoleDto> userRole = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        userRole.forEach(role -> roles.add(role.toString()));
        builder.roles(roles.toArray(new String[]{}));
        @NotNull final UserDetails details = builder.build();
        @NotNull final User userSpring = (User) details;
        @NotNull final CustomUser customUser = new CustomUser(userSpring, user);
        return customUser;
    }

}
