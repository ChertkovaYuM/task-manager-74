package ru.tsc.chertkova.tm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.chertkova.tm.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public class RoleDto extends AbstractModelDto {

    private static final long serialVersionUID = 1;

    @ManyToOne
    private UserDto user;

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USUAL;

    @Override
    public String toString() {
        return roleType.name();
    }

}
