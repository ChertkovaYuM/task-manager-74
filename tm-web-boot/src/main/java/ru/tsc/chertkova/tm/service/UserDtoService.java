package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.IUserDtoRepository;
import ru.tsc.chertkova.tm.enumerated.RoleType;
import ru.tsc.chertkova.tm.exception.user.*;
import ru.tsc.chertkova.tm.model.dto.RoleDto;
import ru.tsc.chertkova.tm.model.dto.UserDto;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", "admin@domain.com", RoleType.ADMIN);
        initUser("test", "test", "test@domain.com", RoleType.USUAL);
        initUser("user", "user", "user@domain.com", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType role
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (email == null || email.isEmpty()) throw new UserEmailEmptyException();
        if (role == null) throw new UserRoleEmptyException();
        final UserDto user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, email, role);
    }

    @Modifying
    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (email == null || email.isEmpty()) throw new UserEmailEmptyException();
        if (roleType == null) throw new UserRoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final UserDto user = new UserDto(login, passwordHash, email);
        @NotNull final RoleDto role = new RoleDto(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @NotNull
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        final UserDto user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException(login);
        return user;
    }

    @NotNull
    public UserDto findById(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        final UserDto user = userRepository.findFirstById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    public UserDto delete(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        final UserDto user = userRepository.findFirstById(userId);
        if (user == null) throw new UserNotFoundException();
        userRepository.deleteById(userId);
        return user;
    }

}
