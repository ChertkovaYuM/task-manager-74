package ru.tsc.chertkova.tm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import ru.tsc.chertkova.tm.model.dto.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    @SneakyThrows
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException
    ) {
        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String value = authException.getMessage();
        @NotNull final Message message = new Message(value);
        writer.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("chertkova");
        super.afterPropertiesSet();
    }

}
