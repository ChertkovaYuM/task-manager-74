package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.model.dto.Result;
import ru.tsc.chertkova.tm.model.dto.UserDto;
import ru.tsc.chertkova.tm.service.UserDtoService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint implements IAuthEndpoint {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDtoService userService;

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/login", produces = "application/json")
    public Result login(
            @NotNull
            @WebParam(name = "login")
            @RequestParam("username") final String login,
            @NotNull
            @WebParam(name = "password")
            @RequestParam("username") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(login, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping(value = "/profile")
    public UserDto profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userService.findByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
