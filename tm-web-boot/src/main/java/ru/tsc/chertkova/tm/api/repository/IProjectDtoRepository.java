package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;

import java.util.List;


public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    @NotNull
    List<ProjectDto> findAll(@NotNull Sort sort);

    @NotNull
    List<ProjectDto> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

    @Modifying
    @Transactional
    void deleteByUserId(@Nullable String userId);

    @NotNull
    List<ProjectDto> findAllByUserId(@Nullable String userId);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDto findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}
