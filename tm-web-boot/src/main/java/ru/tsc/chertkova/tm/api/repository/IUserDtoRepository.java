package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.UserDto;

public interface IUserDtoRepository extends JpaRepository<UserDto, String> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findFirstById(@NotNull String id);

    @Nullable
    UserDto findFirstByEmail(@NotNull String email);

    @Nullable
    @Modifying
    @Transactional
    UserDto deleteByLogin(@NotNull String login);

    @Modifying
    @Transactional
    void deleteById(@Nullable String id);

}
