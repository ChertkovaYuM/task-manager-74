package ru.tsc.chertkova.tm.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class CustomUser extends User {

    @Nullable
    private String userId = null;

    @Nullable
    private String email = null;

    public CustomUser(@NotNull final User user) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
    }

    public CustomUser(@NotNull final User user, @NotNull final UserDto userDTO) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
        userId = userDTO.getId();
        email = userDTO.getEmail();
    }

    public CustomUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(login, password, authorities);
    }

    public CustomUser(
            @NotNull final String login,
            @NotNull final String password,
            final boolean enabled,
            final boolean accountNonExpired,
            final boolean credentialsNonExpired,
            final boolean accountNonLocked,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(login, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

}
