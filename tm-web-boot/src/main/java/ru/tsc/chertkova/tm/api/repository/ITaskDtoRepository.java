package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.TaskDto;

import java.util.List;


public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    @NotNull
    @Query("FROM TaskDto WHERE userId = :userId AND projectId = :projectId")
    List<TaskDto> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDto WHERE userId = :userId AND projectId = :projectId")
    void deleteAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDto WHERE userId = :userId AND projectId IN (:projects)")
    void deleteAllByProjectList(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projects") String[] projects
    );

    @NotNull
    List<TaskDto> findAll(@NotNull Sort sort);

    @NotNull
    List<TaskDto> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

    @Modifying
    @Transactional
    void deleteByUserId(@Nullable String userId);

    @NotNull
    List<TaskDto> findAllByUserId(@Nullable String userId);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDto findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}
