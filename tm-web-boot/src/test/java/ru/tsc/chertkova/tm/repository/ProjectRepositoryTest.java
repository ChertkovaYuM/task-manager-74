package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.tsc.chertkova.tm.api.repository.IProjectDtoRepository;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;

import java.util.Collections;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    private ProjectDto project;

    @Before
    public void setUp() {
        project = new ProjectDto();
        project.setName("Project");
        project.setUserId(USER_ID);
        repository.save(project);
    }

    @After
    public void tearDown() {
        repository.delete(project);
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertTrue(repository.existsByUserIdAndId(USER_ID, project.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(null, project.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER_ID, null));
        Assert.assertFalse(repository.existsByUserIdAndId(null, null));
    }

    @Test
    public void findByUserIdAndId() {
        Assert.assertNotNull(repository.findByUserIdAndId(USER_ID, project.getId()));
        Assert.assertNull(repository.findByUserIdAndId(null, project.getId()));
        Assert.assertNull(repository.findByUserIdAndId(USER_ID, null));
        Assert.assertNull(repository.findByUserIdAndId(null, null));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(1L, repository.countByUserId(USER_ID));
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
        repository.deleteByUserIdAndId(USER_ID, project.getId());
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserId() {
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(null));
        repository.deleteByUserIdAndId(USER_ID, project.getId());
        Assert.assertNotNull(repository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), repository.findAllByUserId(USER_ID));
    }

}
