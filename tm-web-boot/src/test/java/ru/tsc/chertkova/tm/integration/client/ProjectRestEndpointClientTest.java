package ru.tsc.chertkova.tm.integration.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.chertkova.tm.marker.WebCategory;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;
import ru.tsc.chertkova.tm.model.dto.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebCategory.class)
public class ProjectRestEndpointClientTest {

    @NotNull
    private static final String apiUrl  = "http://localhost:8080/api/projects/";

    private static final int COUNT_PROJECTS = 4;

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private static String sessionId;

    @NotNull
    private static List<ProjectDto> projects;

    @NotNull
    private ProjectDto project;

    @BeforeClass
    @SneakyThrows
    public static void setUpClass() {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<ProjectDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, ProjectDto.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Before
    public void setUp() {
        projects = new ArrayList<>();
        @NotNull final String url = apiUrl + "save";
        for (int i = 0; i < COUNT_PROJECTS; i++) {
            @NotNull final ProjectDto projectDto = new ProjectDto();
            projectDto.setName("PROJ_" + i);
            projects.add(projectDto);
        }
        for (ProjectDto projectDto : projects) {
            sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDto>(projectDto, header));
        }
        project = projects.get(0);

    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String clearUrl = apiUrl + "clear";
        sendRequest(clearUrl, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @Test
    public void create() {
        @NotNull final String url = apiUrl + "create";
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(
                url,
                HttpMethod.POST,
                new HttpEntity<>(header)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void existsById() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = apiUrl + "existsById/" + projectId;
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Result> response = template.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(header),
                Result.class);
        Assert.assertNotNull(response.getBody());
        boolean exists = response.getBody().getSuccess();
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = apiUrl + "findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String url = apiUrl + "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDto[]> response =
                (template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), ProjectDto[].class));
        Assert.assertNotNull(response.getBody());
        ProjectDto[] projects = response.getBody();
        Assert.assertTrue(Arrays.stream(projects).anyMatch(p -> project.getId().equals(p.getId())));
    }

    @Test
    public void count() {
        @NotNull final String url = apiUrl + "count";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Long> response =
                template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(3L, response.getBody().longValue());
    }

    @Test
    public void save() {
        @NotNull final String url = apiUrl + "save";
        @NotNull final String projectId = project.getId();
        @NotNull final String expected = "NEW NAME";
        project.setName(expected);
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDto>(project, header));
        @NotNull final String findUrl = apiUrl + "findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(expected, projectDto.getName());
    }

    @Test
    public void delete() {
        @NotNull final String url = apiUrl + "delete";
        @NotNull final String projectId = project.getId();
        @NotNull final String findUrl = apiUrl + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDto>(project, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = apiUrl + "deleteById/" + projectId;
        @NotNull final String findUrl = apiUrl + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAll() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project);
        @NotNull final String projectId = project.getId();
        @NotNull final String url = apiUrl + "deleteAll";
        @NotNull final String findUrl = apiUrl + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projects, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final String projectId = project.getId();
        @NotNull final String url = apiUrl + "clear";
        @NotNull final String findUrl = apiUrl + "findById/" + projectId;
        @NotNull final String findAllUrl = apiUrl + "findAll";
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDto[]> responseEntity =
                template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(header), ProjectDto[].class);
        Assert.assertNotNull(responseEntity.getBody());
        int countProjects = responseEntity.getBody().length;
        Assert.assertEquals(0, countProjects);
    }

}
