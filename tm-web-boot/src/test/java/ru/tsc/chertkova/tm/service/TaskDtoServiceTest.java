package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.TaskNotFoundException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.TaskDto;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskDtoServiceTest {

    @NotNull
    private static String USER_ID;

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private TaskDto task;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskService.clear(USER_ID);
        task = taskService.create(USER_ID);
    }

    @After
    public void tearDown() throws Exception {
        taskService.clear(USER_ID);
    }

    @Test
    public void create() {
        taskService.clear(USER_ID);
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.create(null));
        @NotNull final TaskDto taskDto = taskService.create(USER_ID);
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.existsById(null, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.existsById(USER_ID, null));
        boolean exists = taskService.existsById(USER_ID, task.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.findById(null, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(USER_ID, null));
        @Nullable final TaskDto taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.findAll(null));
        @Nullable final List<TaskDto> tasks = taskService.findAll(USER_ID).stream().collect(Collectors.toList());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void count() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.count(null));
        long count = taskService.count(USER_ID);
        Assert.assertEquals(1L, count);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.save(null, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.save(USER_ID, null));
        @Nullable TaskDto taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, taskDto.getStatus());
        taskDto.setStatus(newStatus);
        taskService.save(USER_ID, taskDto);
        taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.deleteById(null, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.deleteById(USER_ID, null));
        @Nullable TaskDto taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.deleteById(USER_ID, taskDto.getId());
        taskDto = taskService.findById(USER_ID, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.delete(null, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.delete(USER_ID, null));
        @Nullable TaskDto taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.delete(USER_ID, task);
        taskDto = taskService.findById(USER_ID, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void deleteAll() {
        List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task);
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.deleteAll(null, tasks));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.deleteAll(USER_ID, null));
        @Nullable TaskDto taskDto = taskService.findById(USER_ID, task.getId());
        Assert.assertNotNull(taskDto);
        taskService.deleteAll(USER_ID, tasks);
        taskDto = taskService.findById(USER_ID, taskDto.getId());
        Assert.assertNull(taskDto);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.clear(null));
        long count = taskService.count(USER_ID);
        Assert.assertEquals(1L, count);
        taskService.clear(USER_ID);
        count = taskService.count(USER_ID);
        Assert.assertEquals(0L, count);
    }

}
