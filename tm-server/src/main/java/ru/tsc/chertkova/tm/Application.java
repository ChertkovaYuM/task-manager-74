package ru.tsc.chertkova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.chertkova.tm.component.Bootstrap;
import ru.tsc.chertkova.tm.configuration.ContextConfiguration;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ContextConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}
