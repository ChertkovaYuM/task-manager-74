package ru.tsc.chertkova.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.model.AbstractFieldsModel;

@Repository
public interface AbstractUserOwnerModelRepository<M extends AbstractFieldsModel>
        extends AbstractRepository<M> {

}
