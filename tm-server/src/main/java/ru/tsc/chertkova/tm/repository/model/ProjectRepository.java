package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractUserOwnerModelRepository<Project> {

    @Modifying
    @Query("DELETE FROM Project e WHERE e.user.id=:userId")
    void clear(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM Project e WHERE e.user.id=:userId")
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("SELECT e FROM Project e WHERE e.user.id=:userId AND e.id=:id")
    Project findById(@NotNull @Param("userId") String userId,
                     @NotNull @Param("id") String id);

    @Query("SELECT COUNT(e) FROM Project e WHERE e.user.id=:userId")
    int getSize(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM Project e WHERE e.id=:id AND e.user.id=:userId")
    Project removeById(@NotNull @Param("userId") String userId,
                       @NotNull @Param("id") String id);

    @Modifying
    @Query("UPDATE Project e SET e.status=:status WHERE e.user.id=:userId AND e.id=:id")
    void changeStatus(@NotNull @Param("id") String id,
                      @NotNull @Param("userId") String userId,
                      @NotNull @Param("status") String status);

}
